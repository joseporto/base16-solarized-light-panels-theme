Base 16 solarized Dark Theme with Light Panels for Brackets
============================

Based on [Base16 solarized Dark](https://github.com/enricodangelo/brackets-themes/tree/master/enricodangelo.base16-solarized-dark-theme).
